
import './App.css';
import FooterCmp from './components/Footer/FooterCmp';
import Header from './components/Header/Header';
import UploadFile from './components/UploadFile/UploadFile';


function App() {

  return (
    <div className="App-header">
      <Header />
      <UploadFile />
      <FooterCmp />
    </div>
  );
}

export default App;
