import React from 'react'
import { Box, Button, TextInput, FormField, FileInput, Select } from 'grommet';
import { Formik } from "formik";
import { create } from 'ipfs-http-client'
import { useState } from 'react';
import * as Yup from "yup";

function UploadFile() {
    const [fileObj, setfileObj] = useState('');
    const [cid, setCid] = useState('');
    const [optionValue, setOptionValue] = useState('Text Data')
    return (
        <div className='d-flex'>
            <Box
                direction="column"
                // border={{ color: 'white', size: 'large' }}
                pad="large"
            >
                <Formik
                    initialValues={{ name: "" }}
                    onSubmit={async values => {
                        await new Promise(resolve => setTimeout(resolve, 500));
                        const ipfs = create('/ip4/127.0.0.1/tcp/5001')
                        setCid('');
                        if (optionValue === 'File') {
                            const { cid } = await ipfs.add({ path: values.name, content: fileObj })
                            setCid(cid.toString());
                            return;
                        }
                        const { cid } = await ipfs.add(values.name)
                        setCid(cid.toString());
                    }}
                    validationSchema={Yup.object().shape({
                        name: Yup.string()
                            .required(`${optionValue === 'File' ? 'File Name is required' : 'Data is required'}`)
                    })}
                >
                    {({
                        values,
                        touched,
                        errors,
                        dirty,
                        isSubmitting,
                        handleChange,
                        handleBlur,
                        handleSubmit,
                        handleReset
                    }) => (
                        <form
                            onSubmit={event => {
                                event.preventDefault();

                                handleSubmit();
                            }}
                        >

                            <FormField label="Type" error={errors.size}>
                                <Select
                                    className='changeIcon'
                                    name="type"
                                    options={['File', 'Text Data']}
                                    value={optionValue}
                                    size="medium"
                                    onChange={({ option }) => setOptionValue(option)}
                                />
                            </FormField>
                            <FormField label={optionValue === 'File' ? 'File Name' : 'Text Data'}>
                                <TextInput
                                    name="name"
                                    value={values.name}

                                    onChange={handleChange}
                                    onBlur={handleBlur}
                                />

                            </FormField>
                            {errors.name && touched.name && (
                                <div className='error'>{errors.name}</div>
                            )}
                            {optionValue === 'File' && (
                                <FormField>
                                    <FileInput
                                        name="file"
                                        multiple={false}
                                        maxSize={5000}
                                        required
                                        style={{ width: '93%' }}
                                        accept="image/png,image/jpeg"
                                        onChange={event => {
                                            const fileList = event.target.files;
                                            setfileObj(fileList[0])
                                        }}
                                    />
                                </FormField>
                            )}
                            <Box
                                tag="footer"
                                margin={{ top: "medium" }}
                                direction="row"
                                justify="center"
                            >
                                <Button type="submit" color="white" className="text-white" label="Create" />
                            </Box>
                        </form>
                    )}
                </Formik>

                {cid !== '' && (
                    <div className='mt-16'>
                        <a href={`https://ipfs.io/ipfs/${cid}`} rel="noreferrer" target="_blank">View Link</a>
                    </div>
                )}
            </Box>
        </div>
    )
}

export default UploadFile
