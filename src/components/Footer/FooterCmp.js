import { Footer, Text } from 'grommet'
import React from 'react'

const FooterCmp = () => {
    return (
        <Footer className="footerClass" background="white" pad="large">
            <Text>© {new Date().getFullYear()} WebMob Technologies. All Rights Reserved.</Text>
        </Footer>
    )
}

export default FooterCmp
