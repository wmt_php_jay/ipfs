import { Box, Button, Heading } from 'grommet'
import React from 'react'

const Header = () => {
    return (
        <Box
            tag="header"
            direction="row"
            background="brand"
            align="center"
            elevation="large"
            justify="between"
            responsive={false}
            pad={{ vertical: "small" }}
            className='headerBox'
        >
            <Button>
                <Box
                    flex={false}
                    direction="row"
                    align="center"
                    margin={{ left: "small" }}
                >
                    <Heading color="#000" level="4" margin={{ left: "small", vertical: "none" }}>
                        IPFS
                    </Heading>
                </Box>
            </Button>

        </Box>
    )
}

export default Header
